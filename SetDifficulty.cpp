#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include <vector>
#include <string>
#include <random>
#include <ctime>

#include "AM.h"

// Takes the output of "Filter LanguageTool Dict.cpp"

using namespace std;

// *********************************************************

constexpr char aUmlaut = 0xE4;
constexpr char oUmlaut = 0xF6;
constexpr char uUmlaut = 0xFC;
constexpr char eszett =  0xDF;

bool endsWith(const std::string& source, const std::string& fragment) {
    return (source.size() >= fragment.size()) && (source.rfind(fragment) == source.size() - fragment.size());
}

template <typename T> bool containsOnly(const std::set<T> source, T value) {
    return (source.size() == 1) && (source.count(value) == 1);
}

string getShortPluralForm(const string& baseForm, const string& pluralForm) {
    bool umlautAdded = false;

    set<pair<string, string>> rarePlurals = {
        {"us", "i"},
        {"us", "en"},
        {"um", "en"},
        {"a", "en"},
        {"y", "ies"},
        {"er", "e"},
    };

    for (const auto& rarePlural : rarePlurals) {
        if ( (pluralForm.length()-rarePlural.second.length() == baseForm.length()-rarePlural.first.length())
                && endsWith(baseForm, rarePlural.first)
                && endsWith(pluralForm, rarePlural.second)
                && baseForm.substr(0, baseForm.length() - rarePlural.first.length()) == pluralForm.substr(0, pluralForm.length() - rarePlural.second.length())
        ) return "-" + rarePlural.second;
    }

    if (pluralForm.length() < baseForm.length()) return pluralForm;

    string shortPluralForm = "";
    shortPluralForm += '-';

    string _baseForm = baseForm;
    if (endsWith(_baseForm, "\xDF")) {
        _baseForm.replace(_baseForm.size() - 1, 1, "ss");
        shortPluralForm += "ss";
    }

    for (size_t i = 0; i < _baseForm.length(); ++i) {
        char c1 = pluralForm[i];  // must exist since we checked the size before
        char c2 = _baseForm[i];

        if (c1 == c2) {
            continue;
        } else if (!umlautAdded
                    && ( (c1 == aUmlaut && c2 == 'a')||(c1 == oUmlaut && c2 == 'o')||(c1 == uUmlaut && c2 == 'u') )
        ) {
            umlautAdded = true;
            replaceAll(shortPluralForm, "-", "-x");
        }
        else {
            return pluralForm;
        }
    }

    // add extra characters
    if (pluralForm.length() > baseForm.length()) shortPluralForm.append(pluralForm.substr(baseForm.length()));

    return shortPluralForm;
}
// *********************************************************


struct Wort {
    string baseForm;
    set<string> articles;
    set<string> plurals;
    vector<string> sortedArticles;
    vector<string> sortedPlurals;

    string comments;

    int difficulty = 0;

    bool hasSortPlural(const string& form);
};

int main() {
    ifstream inFile{"Wordlist.txt"};
    ofstream outFile{"dummy.txt"};
    string newLine;

    map<string, Wort> worter;

    map<string, string> articleCodes = {{"0", "Das"}, {"1", "Der"}, {"2", "Die"}};

    while(getline(inFile, newLine)) {
        auto parts = tokenize(newLine, "\t");

        if (parts.size() < 3) continue;

        worter[parts[0]].baseForm = parts[0];
        worter[parts[0]].plurals.insert(parts[1]);
        worter[parts[0]].articles.insert(articleCodes.at(parts[2]));
        worter[parts[0]].sortedPlurals.push_back(parts[1]);
        worter[parts[0]].sortedArticles.push_back(articleCodes.at(parts[2]));
    }

    mt19937 rng(time(0));
    //rng.seed(std::random_device()());  // broken on gcc
    uniform_int_distribution<decltype(rng)::result_type> d1000(1, 1000);


    for (auto& x : worter) {

        auto& wort = x.second;
        if (wort.articles.size() > 1 || wort.plurals.size() > 1) {
            wort.comments.append("2+forms, ");
            ++wort.difficulty;
        }

        if (endsWith(wort.baseForm, "ung") && !containsOnly(wort.plurals, wort.baseForm + "en")) {
            wort.comments.append("-ung pl., ");
            ++wort.difficulty;
        }
        if (endsWith(wort.baseForm, "ung") && !containsOnly(wort.articles, string("f"))) {
            wort.comments.append("-ung gend., ");
            ++wort.difficulty;
        }

        if (containsOnly(wort.articles, string("Die"))) {
            for (const auto& pluralForm : wort.sortedPlurals) {
                auto shortPluralForm = getShortPluralForm(wort.baseForm, pluralForm);
                if (shortPluralForm != "-n" && shortPluralForm != "-en" && shortPluralForm != "-nen") {
                    wort.comments.append("strange f. plur, ");
                    wort.difficulty += 1;
                    if (shortPluralForm != "-s") {
                        wort.comments.append("very strange f. plur, ");
                        wort.difficulty += 1;
                    }
                } else if (wort.sortedPlurals.size() == 1 && endsWith(wort.baseForm, "e")) {
                    wort.comments.append("easy f. plur, ");
                    wort.difficulty -= 1;
                } else if (wort.sortedPlurals.size() == 1 && (endsWith(wort.baseForm, "ung") || endsWith(wort.baseForm, "ei") || endsWith(wort.baseForm, "in") || endsWith(wort.baseForm, "keit") || endsWith(wort.baseForm, "schaft"))) {
                    wort.comments.append("trivial f. plur, ");
                    wort.difficulty -= 2;
                }
            }
        }

        if (endsWith(wort.baseForm, "e") && !containsOnly(wort.articles, string("Die"))) {
            wort.comments.append("non-f baseform in -e, ");
            wort.difficulty += 1;
        } else if (wort.plurals.count(wort.baseForm + "n") > 0 && !containsOnly(wort.articles, string("Die"))) {
            wort.comments.append("non-f plur in -n, ");
            wort.difficulty += 1;
        }

        if (endsWith(wort.baseForm, "er")) {
            if (containsOnly(wort.plurals, wort.baseForm)) {
                wort.comments.append("common plur of -er");
                wort.difficulty -= 1;
            } else {
                wort.comments.append("uncommon plur of -er");
                wort.difficulty += 1;
            }
        }

        for (const auto& pluralForm : wort.sortedPlurals) {
            auto shortPluralForm = getShortPluralForm(wort.baseForm, pluralForm);
            if (shortPluralForm.at(0) != '-') {
                wort.comments.append("irregular plural");
                wort.difficulty += 2;
            } else if (shortPluralForm == "-i") {
                wort.comments.append("plural in -i");
                wort.difficulty += 1;
            }
        }

        if (wort.difficulty > 3 && d1000(rng) > 500) cout << wort.baseForm << "\t" << *wort.plurals.rbegin() << "\t" << wort.comments << "\t" << wort.difficulty << "\n";
        for (size_t i = 0; i < wort.sortedPlurals.size(); ++i) {
            //if (d1000(rng) > 975) cout << wort.baseForm << "\t" << wort.sortedPlurals[i] << "\t" << getShortPluralForm(wort.baseForm, wort.sortedPlurals[i]) << "\n";
            outFile << wort.baseForm << "\t" << wort.sortedPlurals[i] << "\t" << wort.sortedArticles[i] << "\t" << wort.difficulty << "\n";
        }
    }

    map<int, size_t> difficultyCount;
    for (const auto& x : worter) {
        const auto& wort = x.second;
        if (difficultyCount.count(wort.difficulty) == 0)
            difficultyCount[wort.difficulty] = 1;
        else
            difficultyCount.at(wort.difficulty) += 1;
    }
    for (const auto& x : difficultyCount) {
        cout << x.first << "   " << x.second << "\n";
    }

    return 0;
}

#ifndef AM_SMALL_FUNCTIONS
#define AM_SMALL_FUNCTIONS

#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <memory>
#include <cstdarg>  // va_start

// *****************************************************************************
// Fixes for C++11 missing features of this gcc version
// *****************************************************************************

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template <typename T> std::string to_string(T number) {
        std::stringstream ss{""};
        ss << number;
        return ss.str();
}

// *****************************************************************************
// *****************************************************************************

template <typename T> std::string to_string(T number, const std::string& name, const std::string& pluralForm = std::string("")) {
    if (number != 1 && pluralForm == "")   return to_string(number) + " " + name + "s";
    if (number != 1)   return to_string(number) + " " + pluralForm;
    return to_string(number) + " " + name;
}

//double to_double(const std::string& s, double defaultValue);

template <typename T> T to_number(std::string str, T defaultValue)  {
    std::istringstream ss{str};
    T x;
    ss >> x;
    if (ss.fail()) return defaultValue;
    return x;
}

void replaceAll(std::string& str, const std::string& from, const std::string& to);
std::vector<std::string> tokenize(const std::string& source, const std::string& delimiter, bool removeEmptyTokens = false);
std::string join(const std::vector<std::string>& tokens, const std::string& delimiter);
std::string strprintf(const std::string format, ...);
std::string to_lower(const std::string& x);
std::string to_upper(const std::string& x);

bool fileExists(const std::string& fileName);
void backupFile(const std::string& fileName);


template <typename T> void dirtyLog(const T& data) {
#ifdef AM_DIRTY_LOG
    constexpr auto logName = "D:\\dirtyLog.txt";
    if (fileExists(logName)) backupFile(logName);
    std::ofstream log{logName};
    log << data;
#endif
    return;
}

#endif // AM_SMALL_FUNCTIONS

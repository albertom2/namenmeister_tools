#include <iostream>
#include <fstream>
#include <set>
#include <unordered_set>
#include <string>
#include <vector>
#include <map>

using namespace std;

void to_lower(string& word) {
    for (auto& c : word) {
        if (     (c >= 'A' && c <= 'Z')
              || (c >= '\xc0' /* 'À' in Windows-1252 */ && c <= '\xde' /* 'Þ' in Windows-1252 */ )
            ) {
            c += 'a' - 'A';
        }
    }
}

int main() {

    ifstream inFile{"german.dic"};
    ofstream outFile{"long list.csv"};

    cout << "Catalogo i nomi" << endl;

    set<string> all, names, lowerCaseNames, verbRadices;
    map<string, string> decompositions;

    size_t count = 0;

    while(inFile.good()) {
        string newWord;
        inFile >> newWord;

        if (newWord.size() < 3) continue;

        if (     (newWord.at(0) >= 'A' && newWord.at(0) <= 'Z')
              || (newWord.at(0) >= '\xc0' /* 'À' in Windows-1252 */ && newWord.at(0) <= '\xde' /* 'Þ' in Windows-1252 */ )
            ) {
            names.insert(newWord);
            string newWordCopy{newWord};
            to_lower(newWordCopy);
            lowerCaseNames.insert(newWordCopy);
        } else if (newWord.rfind("en") == newWord.size() - 2) {
            newWord = newWord.substr(0, newWord.size() - 2);
            //cout << newWord << endl;
            verbRadices.insert(newWord);
        }

        to_lower(newWord);
        all.insert(newWord);

        if ((count++ & 0xFFFF) == 0) cout << '.';
    }

    ifstream inFile2{"extra.dic"};
    while(inFile2.good()) {
        string newWord;
        inFile2 >> newWord;
        names.insert(newWord);

        to_lower(newWord);
        lowerCaseNames.insert(newWord);
        all.insert(newWord);

        if ((++count & 0xFFFF) == 0) cout << '.';
    }

    cout << endl;
    cout << "Scompongo i nomi" << endl;

    count = 0;
    size_t primitiveCount = 0;

    for (const auto& name : names) {
        bool decomposed = false;

        if (name.size() < 3) continue;
        if (name < string("Adress")) continue;

        static const vector<string> trivialSuffixes {"e", "s", "n", "er", "en", "es", "in", "innen"};
        // Tenta tutte le possibili decomposizioni
        for (const auto& trivialSuffix : trivialSuffixes) {

            if (name.size() < trivialSuffix.size() + 3) continue;
            string radix, suffix;
            radix = name.substr(0, name.size() - trivialSuffix.size());
            suffix = name.substr(name.size() - trivialSuffix.size());

            if (suffix == trivialSuffix && names.count(radix) == 1) {
                decomposed = true;
                //decompositions[name] = radix + " + " + suffix;
                goto endDecomposition;
            }
        }

        static const vector<string> verbGenitives {"ens"};
        for (const auto& verbGenitive : verbGenitives) {
            // Tenta tutte le possibili decomposizioni
            if (name.size() < verbGenitive.size() + 4) continue;
            string radix, suffix;
            radix = name.substr(0, name.size() - verbGenitive.size());
            suffix = name.substr(name.size() - verbGenitive.size());

            to_lower(radix);

            if (suffix == verbGenitive && verbRadices.count(radix) == 1) {
                decomposed = true;
                //decompositions[name] = radix + " + " + verbGenitive;
                goto endDecomposition;
            }
        }

        static const vector<string> sureVerbSuffixes {"erns", "elns"};
        for (const auto& verbSuffix : sureVerbSuffixes) {
            if (name.size() < verbSuffix.size() + 3) continue;
            string suffix;
            suffix = name.substr(name.size() - verbSuffix.size());

            if (suffix == verbSuffix) {
                decomposed = true;
                //decompositions[name] = radix + " + " + suffix;
                goto endDecomposition;
            }
        }


        for (size_t i = 2; i+2 < name.size(); ++i) {

            auto firstPart = name.substr(0, i);
            to_lower(firstPart);
            if (all.count(firstPart) == 0 && verbRadices.count(firstPart) == 0) continue;

            auto secondPart = name.substr(i);

            if (lowerCaseNames.count(secondPart) == 1) {
                decomposed = true;
                //decompositions[name] = firstPart + " + " + secondPart;
                break;
            }

            if (secondPart.at(0) == 's' && secondPart.size() > 3) {
                secondPart.erase(0,1);
                if (lowerCaseNames.count(secondPart) == 1) {
                    decomposed = true;
                    //decompositions[name] = firstPart + " + s + " + secondPart;
                    break;
                }
            }
        }

        endDecomposition:;

        if (decomposed == true /*decompositions.count(name) == 1*/) {
            //outFile << name << " -> " << decompositions.at(name);
        } else {
            outFile << name << '\n';
            ++primitiveCount;
        }
        if ((++count & 0xFFFF) == 0) cout << '.';
    }

    cout << "-> " << primitiveCount << " <-" << endl;

    return 0;
}

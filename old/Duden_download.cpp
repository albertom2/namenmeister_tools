#include <cstdio>
#include <iostream>
#include <fstream>
#include <set>
#include <algorithm>
#include <memory>
#include <sstream>
#include <thread>
#include <random>
#include <curl/curl.h>

using namespace std;

std::string toFileName(const std::string& word) {
    return "Datafiles\\" + word + ".html";
}

bool endsWith(const string& word, const string& suffix) {
    return (word.size() >= suffix.size()) && (word.rfind(suffix) == word.size() - suffix.size())
}

void downloadWord(const std::string& word) {
    CURL* curl;
    CURLcode res;

    curl = curl_easy_init();
    if(curl) {
        auto url =  "http://www.duden.de/rechtschreibung/" + word;   // I hate how is in Latin-1, but...
        auto fileName = toFileName(word);

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        /* example.com is redirected, so we tell libcurl to follow redirection */
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        auto fp = fopen(fileName.c_str(), "w");
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        /* Perform the request, res will get the return code */
        cout << "Attempting to download " << url << " into " << fileName << endl;
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK) throw runtime_error(string("curl_easy_perform() failed:") + curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);
    }
}

int main(void) {
    vector<string> words;
    set<string> alreadyDownloaded;

    stringstream inData;
    {
        ifstream inFile("THE RESULT!.txt");
        inData << inFile.rdbuf();
    }

    while (inData.good()) {
        string newWord;
        getline(inData, newWord);

        if (endsWith(word, "ung")) continue;
        if (endsWith(word, "keit")) continue;
        if (endsWith(word, "ismus")) continue;
        if (endsWith(word, "schaft")) continue;
        if (endsWith(word, "chen")) continue;
        if (endsWith(word, "heit")) continue;
        if (endsWith(word, "t\xE4t")) continue; // t�t
        //if (endsWith(word, "tum")) continue; //NO! Irrtum!

        {
            // Check if word was already downloaded, skip it in the case
            ifstream test(toFileName(newWord));
            if (test.good()) continue;
        }

        words.push_back(newWord);
    }

    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> distribution(0, words.size());

    int downloadedPages = 0;

    try {
        while (downloadedPages < 100) {
            auto word = words.at(distribution(generator));

            // Check if word was already downloaded, skip it in the case
            ifstream test(toFileName(word));
            if (test.good()) continue;

            cout << "(" << downloadedPages << ")" << " Downloading " << word << endl;
            downloadWord(word);
            std::this_thread::sleep_for(std::chrono::seconds{1});
            ++downloadedPages;
        }
    } catch (const runtime_error& e) {
        cout << e.what() << endl;
        return 1;
    }

    return 0;
}

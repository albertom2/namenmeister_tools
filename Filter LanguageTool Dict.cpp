#include <iostream>
#include <unordered_set>
#include <set>

#include "AM.h"

using namespace std;

int main() {
    stringstream _dictTxt;
    ofstream outFile{R"(datasource\MyList.txt)", ios::trunc};

    {
        ifstream dictTxt{R"(datasource\LanguageTool-dict3.txt)"};
        _dictTxt << dictTxt.rdbuf();
    }

    string line;
    unordered_set<string> dict;

    string lastPrintedWord;

    size_t words = 0;
    while (getline(_dictTxt, line)) {
        auto parts = tokenize(line,"\t");

        if (line[0] == 'a') {break;}

        if (parts[2].find("SUB") == string::npos) continue;
        if (parts[2].find("NOM") == string::npos) continue;
        if (parts[2].find("PLU") == string::npos) continue;
        if (parts[2].find("ADJ") != string::npos) continue;

        int gender;
        if (parts[2].find("NEU") != string::npos) {
            gender = 0;
        } else if (parts[2].find("MAS") != string::npos) {
            gender = 1;
        } else if (parts[2].find("FEM") != string::npos) {
            gender = 2;
        } else {
            continue;
        }

        outFile << parts[1] << "\t" << parts[0] << "\t" << gender << "\n";

        if ((++words)%1000 == 0) cout << ".";
    }
    return 0;

}

﻿#include "AM.h"

using namespace std;

// http://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string
void replaceAll(string& str, const string& from, const string& to) {
    if(from.empty()) return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

vector<string> tokenize(const string& source, const string& delimiter, bool removeEmptyTokens) {
    if (delimiter.size() < 1) throw(runtime_error("Programming bug"));

    vector<string> tokens;
    size_t start, end;

    start = 0;
    while (true) {
        end = source.find(delimiter, start);
        if (end == string::npos) {   // End of string reached
            tokens.push_back(source.substr(start, string::npos));

            if (removeEmptyTokens) tokens.erase(remove(tokens.begin(), tokens.end(), ""), tokens.end());
            return tokens;
        }

        tokens.emplace_back(source.substr(start, end - start));
        start = end + delimiter.size();
    }
}

// http://stackoverflow.com/questions/5689003/how-to-implode-a-vector-of-strings-into-a-string-the-elegant-way
std::string join(const std::vector<std::string>& tokens, const std::string& delimiter) {
    stringstream _result{tokens.front()};
    for (auto it = tokens.begin(); it != tokens.end(); ++it) {
        if (it != tokens.begin()) _result << delimiter;
        _result << *it;
    }

    return _result.str();
}

// http://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
// With some modifications
std::string strprintf(const std::string format, ...) {
    va_list arguments;
    va_start(arguments, format);
    size_t outputLength = vsnprintf(nullptr, 0, format.c_str(), arguments);
    va_end(arguments);

    if (outputLength < 0) throw std::runtime_error("Error writing string " + format);

    ++outputLength; // accommodate the terminator
    std::string output(outputLength, '\0');
    //output.resize(outputLength);

    va_start(arguments, format);
    // Only safe in C++11 and above
    vsnprintf(&output[0], outputLength, format.c_str(), arguments);
    va_end(arguments);

    // Drop the terminator
    output.resize(output.size()-1);

    return output;
}

std::string to_lower(const std::string& x) {
    std::string result = x;
    for (char& c : result) if (c >= 'A' && c <= 'Z') c += 'a' - 'A';
    return result;
}

std::string to_upper(const std::string& x) {
    std::string result = x;
    for (char& c : result) if (c >= 'a' && c <= 'z') c -= 'a' - 'A';
    return result;
}


bool fileExists(const string& fileName) {
    // http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
    ifstream test{fileName.c_str()};
    return test.good();
}


void backupFile(const std::string& fileName) {
    string data;

    {
        ifstream file{fileName.c_str(), ios::binary};

        if (!file) {
            throw (runtime_error("Impossibile to backup " + fileName));
        }

        stringstream _data;
        _data << file.rdbuf();
        data = _data.str();
    }

    size_t pointOfInsertion = fileName.rfind(".");
    if (pointOfInsertion == string::npos) pointOfInsertion = fileName.length();

    size_t suffix = 0;
    bool backupDone = false;
    while (!backupDone) {
        if (suffix > 99999) throw (runtime_error("Impossibile to backup " + fileName + " : too many backups already existing!"));

        string backupFileName = fileName;
        backupFileName.insert(pointOfInsertion, ".bk" + to_string(suffix));

        if (fileExists(backupFileName)) {++suffix; continue;}

        ofstream backupFile(backupFileName.c_str(), ios::binary);
        backupFile << data;

        if (backupFile.fail()) throw (runtime_error("Impossibile to backup " + fileName));
        backupDone = true;
    }

    return;
}







